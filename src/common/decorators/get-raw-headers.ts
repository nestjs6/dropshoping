import { createParamDecorator } from '@nestjs/common';

export const GetRawDecorator = createParamDecorator((data, req) => {
  const request = req.switchToHttp().getRequest();

  return request.rawHeaders;
});
