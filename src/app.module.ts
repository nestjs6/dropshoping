import { Logger, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './services/user/user.module';
import { CommonModule } from './common/common.module';
// import { RolesModule } from './services/roles/roles.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      isGlobal: true,
    }),
    MongooseModule.forRoot(`${process.env.MONGO_DB}`),
    UserModule,
    CommonModule,
    // RolesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  private readonly logger = new Logger(AppModule.name);
  constructor() {
    this.logger.verbose('========================================');
    this.logger.verbose(`* compilación:       ==>  Started`);
    this.logger.verbose('========================================');
  }
}
