import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'users', versionKey: false, timestamps: true })
export class User extends Document {
  @Prop()
  name: string;

  @Prop()
  lastname: string;

  @Prop({
    select: false,
  })
  password: string;

  @Prop({ required: true, unique: true, lowercase: true })
  email: string;

  @Prop()
  image: string;

  @Prop({
    default: 0,
  })
  attempts: number;

  @Prop({
    default: true,
  })
  status: boolean;

  @Prop({
    array: true,
    default: [],
  })
  roles: number[];

  @Prop({
    array: true,
    default: [],
  })
  project: number[];
}

export const UserSchema = SchemaFactory.createForClass(User);
