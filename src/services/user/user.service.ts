import { Injectable } from '@nestjs/common';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ResponseDTO } from 'src/common/dto';

import * as bcrypt from 'bcrypt';
import { CreateUserDto, LoginUserDto, UpdateUserDto } from './dto';
import { JwtService } from '@nestjs/jwt';
import { IJwtPayload } from './interfaces/jwt.interfaces';
import { DefaultResponse } from 'src/helpers/common';
import { User } from './entities';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly usersService: Model<User>,
    private readonly jwtService: JwtService,
  ) {}
  async create(createAuthDto: CreateUserDto): Promise<ResponseDTO> {
    createAuthDto.password = bcrypt.hashSync(createAuthDto.password, 10);

    const user = await this.usersService.create(createAuthDto);

    return DefaultResponse.sendCreated('Usuario creado', {
      user,
      token: this.getJwtToken({ id: user._id }),
    });
  }

  async signin(loginSigninDto: LoginUserDto): Promise<ResponseDTO> {
    const { email, password } = loginSigninDto;
    const user = await this.usersService.findOne({ email: email }).select({
      name: 1,
      lastname: 1,
      password: 1,
      email: 1,
      image: 1,
      attempts: 1,
      status: 1,
      roles: 1,
    });

    if (!bcrypt.compareSync(password, user.password)) {
      return DefaultResponse.sendUnauthorized(
        'Ingrese los datos correctamente',
      );
    }

    return DefaultResponse.sendOk('true', {
      user,
      token: this.getJwtToken({ id: user._id }),
    });
  }

  async findAll(): Promise<ResponseDTO> {
    const user = await this.usersService.find();
    return DefaultResponse.sendOk('Lista de usuarios', user);
  }

  async findOneById(id: string): Promise<ResponseDTO> {
    const user = await this.usersService.findById(id);
    if (!user) {
      return DefaultResponse.sendNotFound('No se encontró el usuario');
    }
    return DefaultResponse.sendOk('Usuario encontrado', user);
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const user = await this.usersService.findByIdAndUpdate(id, updateUserDto);
    return DefaultResponse.sendOk('Usuario actualizado', user);
  }

  async remove(id: string) {
    await this.usersService.deleteOne({ _id: id });

    return DefaultResponse.sendOk('Usuario eliminado');
  }

  private getJwtToken(payload: IJwtPayload) {
    const token = this.jwtService.sign(payload);
    return token;
  }
}
