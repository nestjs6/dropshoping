import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { DefaultResponse } from 'src/helpers/common';

export const GetUserDecorator = createParamDecorator(
  (data, req: ExecutionContext) => {
    const request = req.switchToHttp().getRequest();
    const user = request.user;

    if (!user) DefaultResponse.sendInternalServerEE('Usuario no encontrado');

    return !data ? user : user[data];
  },
);
