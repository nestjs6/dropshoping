import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ParseMongoIdPipe } from 'src/common/pipes/parse-mongo-id/parse-mongo-id.pipe';
import { CreateUserDto, LoginUserDto, UpdateUserDto } from './dto';
import { RolesGuard } from './guard/roles.guard';
import { Auth } from './decorator/roles.decorator';

@Controller('auth')
export class UserController {
  constructor(private readonly authService: UserService) {}

  @Post('signup')
  async create(@Body() createAuthDto: CreateUserDto) {
    return await this.authService.create(createAuthDto);
  }

  @Post('signin')
  async signin(@Body() loginSigninDto: LoginUserDto) {
    return await this.authService.signin(loginSigninDto);
  }

  @Get()
  @UseGuards(RolesGuard)
  @Auth('admin', 'user')
  async findAll() {
    return await this.authService.findAll();
  }

  @Get(':id')
  async findOneById(@Param('id', ParseMongoIdPipe) id: string) {
    return await this.authService.findOneById(id);
  }

  @Patch(':id')
  async update(
    @Param('id', ParseMongoIdPipe) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return await this.authService.update(id, updateUserDto);
  }

  @Delete(':id')
  async remove(@Param('id', ParseMongoIdPipe) id: string) {
    return await this.authService.remove(id);
  }
}
