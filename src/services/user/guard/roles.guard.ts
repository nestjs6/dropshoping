import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const roles: string[] = this.reflector.get<string[]>(
      'roles',
      context.getHandler(),
    );
    console.log(roles);

    // const request = context.switchToHttp().getRequest();
    // const user = request.user as User;
    // if (!user) {
    //   DefaultResponse.sendBadRequest('Usuario no encontrado');
    // }
    return false;
  }
}
