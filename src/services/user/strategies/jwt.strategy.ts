import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { User } from '../entities/user.entity';
import { IJwtPayload } from '../interfaces/jwt.interfaces';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ConfigService } from '@nestjs/config';

import { Injectable } from '@nestjs/common';
import { DefaultResponse } from 'src/helpers/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectModel(User.name)
    private readonly userService: Model<User>,

    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_BACKEND'),
    });
  }
  async validate(payload: IJwtPayload): Promise<User> {
    const { id } = payload;
    const user = await this.userService.findOne({ _id: id });

    if (!user) DefaultResponse.sendUnauthorized('Token no valido');

    if (!user.status) DefaultResponse.sendUnauthorized('Usuario inactivo');

    return user;
  }
}
