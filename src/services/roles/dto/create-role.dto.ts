import { IsNumber, IsPositive, IsString, MinLength } from 'class-validator';

export class CreateRoleDto {
  @IsString()
  @MinLength(5)
  name: string;

  @IsNumber()
  @IsPositive()
  key: number;
}
