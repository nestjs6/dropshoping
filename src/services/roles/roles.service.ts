import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Model } from 'mongoose';
import { Role } from './entities/role.entity';
import { DefaultResponse } from 'src/helpers/common';

@Injectable()
export class RolesService {
  constructor(private readonly roleService: Model<Role>) {}
  async create(createRoleDto: CreateRoleDto) {
    const role = await this.roleService.create(createRoleDto);
    return DefaultResponse.sendOk('Rol creado', role);
  }

  findAll() {
    return `This action returns all roles`;
  }

  findOne(id: number) {
    return `This action returns a #${id} role`;
  }

  update(id: string, updateRoleDto: UpdateRoleDto) {
    console.log(updateRoleDto);

    return `This action updates a #${id} role`;
  }

  remove(id: number) {
    return `This action removes a #${id} role`;
  }
}
