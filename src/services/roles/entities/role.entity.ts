import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ collection: 'role', versionKey: false })
export class Role extends Document {
  @Prop({
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  })
  name: string;
  @Prop({
    type: Number,
    required: true,
    unique: true,
  })
  key: number;
  @Prop({
    default: true,
  })
  status: boolean;
}

export const RoleSchema = SchemaFactory.createForClass(Role);
