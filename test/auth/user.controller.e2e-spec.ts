import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import { DefaultResponse } from '../../src/helpers/common/response.helper';

describe('UserController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('Testing users', () => {
    it('Create user', async () => {
      const response = await request(app.getHttpServer())
        .post('/auth')
        .send({
          name: 'Admin',
          lastname: 'Admin',
          password: 'testing',
          email: 'admin14@gmail.com',
          image: 'string',
        })
        .expect(201);

      expect(response.body.success).toBe(true);
      expect(response.body.message).toBe('Usuario creado');
      expect(response.body.statusCode).toBe(201);
      expect(DefaultResponse.sendOk('Usuario creado', response.body.data));
    });

    it('List all users', async () => {
      const response = await request(app.getHttpServer())
        .get('/auth')
        .expect(200);

      expect(response.body.success).toBe(true);
      expect(response.body.message).toBe('Lista de usuarios');
    });

    it('List user for id', async () => {
      const response = await request(app.getHttpServer())
        .get('/auth/65f4d85e7492e8787fbfeb70')
        .expect(200);

      expect(response.body.success).toBe(true);
      expect(response.body.message).toBe('Usuario encontrado');
      expect(DefaultResponse.sendOk('Usuario encontrado', response.body.data));
    });
  });
});
